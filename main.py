import configparser
from workflow.tasks.etl.pandas_etl import PandasETL
from workflow.tasks.etl.spark_etl import SparkETL
from workflow.tasks.utils.utilities import Utilities

if __name__ == "__main__":

    config = configparser.ConfigParser()
    config.read(Utilities.get_path('/conf/conf.ini'))
    if config['DEFAULT']['UseSpark'] == 'true':
        etl = SparkETL(config)
        etl.run()
        etl.extract_journal_with_most_drugs()
    else:
        etl = PandasETL(config)
        etl.run()
        etl.extract_journal_with_most_drugs()
