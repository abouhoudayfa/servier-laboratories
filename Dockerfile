FROM puckel/docker-airflow:1.10.9

USER root
WORKDIR /usr/local/airflow
ENV AIRFLOW_HOME=/usr/local/airflow
COPY requirements.txt ./requirements.txt

RUN apt-get update && pip install --upgrade pip && pip install -r requirements.txt

RUN rm ./requirements.txt
