import ast
import json

import pandas as pd

from workflow.tasks.etl.etl import ETL
from workflow.tasks.utils.utilities import Utilities


class PandasETL(ETL):

    def __init__(self, conf):
        self.conf = conf['DEFAULT']

    def read_pubmed(self, csv_delimiter, data_pm_json, data_pubmed_csv, json_orient, date_column):
        """
        Unified method to parse json and csv file for pubmed data
        :param csv_delimiter: how csv files are delimited
        :param data_pm_json: pubmed json file
        :param data_pubmed_csv: pubmed csv file
        :param json_orient: how the data is represented in json file
        :param date_column: date column name in pubmed input data
        :return: pandas dataframe
        """
        pm_json = pd.read_json(json.loads(Utilities.json_cleaner_loader(data_pm_json)),
                               orient=json_orient)
        pm_json[date_column] = pm_json[date_column].astype(str)
        pm_csv = pd.read_csv(data_pubmed_csv, sep=csv_delimiter)

        return pm_csv.append(pm_json)

    def run(self):
        """
        This method implements the transform and load steps in this ETL class
        :return: None
        """

        # get variables from conf
        data_pm_json = Utilities.get_path(self.conf['PubMedJsonFile'])
        json_orient = self.conf['JsonOrient']
        data_pubmed_csv = Utilities.get_path(self.conf['PubMedCsvFile'])
        data_drugs_csv = Utilities.get_path(self.conf['DrugsCsvFile'])
        trials_csv = Utilities.get_path(self.conf['ClinicalTrialsCsvFile'])
        csv_delimiter = ast.literal_eval(self.conf['CsvDelimiter'])
        date_col = self.conf['DateColumn']
        journal_col = self.conf['JournalColumn']
        drug_col = self.conf['DrugColumn']
        id_col = self.conf['IdColumn']
        ct_title_col = self.conf['ClinicalTrialTitleColumn']
        pm_title_col = self.conf['PubMedTitleColumn']
        output_file = self.conf['OutputFile']

        # data extract
        pm = self.read_pubmed(csv_delimiter, data_pm_json, data_pubmed_csv, json_orient, date_col)
        drugs = pd.read_csv(data_drugs_csv, sep=csv_delimiter)
        ct = pd.read_csv(trials_csv, sep=csv_delimiter)

        # data cleansing
        ct[date_col] = ct[date_col].map(lambda x: Utilities.convert_date(x))
        ct[journal_col].replace({r'\\\w+': ''}, regex=True, inplace=True)
        pm[date_col] = pm[date_col].map(lambda x: Utilities.convert_date(x))

        # data transformation
        ct_drug = Utilities.get_pandas_drug_reference(drugs, drug_col, ct, ct_title_col,
                                                      [id_col, date_col, journal_col])
        pm_drug = Utilities.get_pandas_drug_reference(drugs, drug_col, pm, pm_title_col,
                                                      [id_col, date_col, journal_col])

        # the result is a merged dataframe containing for each drug and journal the id and title of
        # either pubmed or clinical_trial
        res = ct_drug.merge(pm_drug, on=[drug_col, journal_col], how='outer',
                            suffixes=['_clinical_trial', '_pubmed']).sort_values(by=[journal_col, drug_col])

        # data load
        res.to_json(Utilities.get_path(output_file), orient='records')

    def extract_journal_with_most_drugs(self) -> None:
        """
        This method aims to retrieve the journal that references the maximum of different drugs
        :return: None
        """
        # read the output json of run method
        data = pd.read_json(Utilities.get_path(self.conf['OutputFile']),
                            orient=self.conf['JsonOrient'])
        journal_col = self.conf['JournalColumn']
        drug_col = self.conf['DrugColumn']

        # get the journals that referenced the max of different drugs
        df = data[[journal_col, drug_col]] \
            .drop_duplicates() \
            .groupby(by=[journal_col]) \
            .count() \
            .sort_values(by=[drug_col], ascending=False)
        print(df[df[drug_col] == df[drug_col].max()])
