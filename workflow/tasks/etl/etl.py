class ETL:
    def run(self) -> None:
        """
        This method implements the transform and load steps in this ETL class
        :return: None
        """
        pass

    def extract_journal_with_most_drugs(self) -> None:
        """
        This method aims to retrieve the journal that references the maximum of different drugs
        :return: None
        """
        pass
