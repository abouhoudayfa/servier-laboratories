import ast
import os

from workflow.tasks.etl.etl import ETL
from workflow.tasks.utils.utilities import Utilities


class SparkETL(ETL):

    def __init__(self, conf):
        self.default_conf = conf['DEFAULT']
        self.spark_conf = conf['spark.conf']
        self.clean_json = 'tmp.json'
        self.spark = self.start_spark_session()

    def read_pubmed(self, csv_delimiter, data_pm_json, data_pubmed_csv,
                    date_col, journal_col, id_col, title_col, clean_json):
        """
        Unified method to parse json and csv file for pubmed data
        :param csv_delimiter: how csv files are delimited
        :param data_pm_json: pubmed json file
        :param data_pubmed_csv: pubmed csv file
        :param date_col: date column name in pubmed input data
        :param journal_col: journal column
        :param id_col: id column
        :param title_col: title column
        :param clean_json: json used temporarly to read a clean json
        :return: spark dataframe
        """
        pm_csv = Utilities.spark_read_csv(self.spark, csv_delimiter, data_pubmed_csv)
        pm_json = Utilities.spark_read_json(self.spark, data_pm_json, id_col, title_col, date_col, journal_col,
                                            clean_json)
        return pm_csv.union(pm_json)

    def run(self):
        """
        This method implements the transform and load steps in this ETL class
        :return: None
        """

        def date_format_query(title):
            return """select id, {},
                      case when date like '%-%' then TO_DATE(CAST(UNIX_TIMESTAMP(date, 'yyyy-dd-MM') AS TIMESTAMP))
                           when date like '%/%' then TO_DATE(CAST(UNIX_TIMESTAMP(date, 'dd/MM/yyyy') AS TIMESTAMP))
                           else TO_DATE(CAST(UNIX_TIMESTAMP(date, 'd MMMM yyyy') AS TIMESTAMP))
                      end as date,""".format(title)

        def get_drug_in_pub(table, alias, title_col):
            return """select * from {} {}
                    cross join (select distinct drug from drugs) as d
                    where lower({}) like CONCAT('%',lower(d.drug),'%')""".format(table, alias, title_col)

        # get variables from conf
        data_pm_json = Utilities.get_path(self.default_conf['PubMedJsonFile'])
        data_pubmed_csv = Utilities.get_path(self.default_conf['PubMedCsvFile'])
        data_drugs_csv = Utilities.get_path(self.default_conf['DrugsCsvFile'])
        trials_csv = Utilities.get_path(self.default_conf['ClinicalTrialsCsvFile'])
        csv_delimiter = ast.literal_eval(self.default_conf['CsvDelimiter'])
        date_col = self.default_conf['DateColumn']
        id_col = self.default_conf['IdColumn']
        title_col = self.default_conf['PubMedTitleColumn']
        journal_col = self.default_conf['JournalColumn']

        # data extract and create temp view to use spark sql for next steps
        pm = self.read_pubmed(csv_delimiter, data_pm_json, data_pubmed_csv, date_col, journal_col,
                              id_col, title_col, self.clean_json)
        pm.createOrReplaceTempView('pm')
        drugs = Utilities.spark_read_csv(self.spark, csv_delimiter, data_drugs_csv)
        drugs.createOrReplaceTempView('drugs')

        ct = Utilities.spark_read_csv(self.spark, csv_delimiter, trials_csv)
        ct.createOrReplaceTempView('ct')

        # data cleansing
        ct_transfrom = self.spark.sql(date_format_query('scientific_title') +
                                      " regexp_replace(regexp_replace(journal, '\\\\\\\\', ''), 'xc3x28', '') "
                                      "as journal from ct")
        ct_transfrom.createOrReplaceTempView("clean_ct")

        pm_transform = self.spark.sql(date_format_query('title') + """journal from pm""")
        pm_transform.createOrReplaceTempView("clean_pm")

        # data transformation
        pm_drug = self.spark.sql(get_drug_in_pub('clean_pm', 'p', 'p.title'))
        pm_drug.createOrReplaceTempView("pm_drug")
        ct_drug = self.spark.sql(get_drug_in_pub('clean_ct', 'c', 'c.scientific_title'))
        ct_drug.createOrReplaceTempView("ct_drug")

        # the result is a merged dataframe containing for each drug and journal the id and title of
        # either pubmed or clinical_trial
        res = self.spark.sql(""" select case when cd.drug is not null then cd.drug else pd.drug end as drug, 
                                        case when cd.journal is not null then cd.journal else pd.journal end as journal,
                                        cd.id as clinical_trial_id,
                                        cd.date as clinical_trial_date,
                                        pd.id as pubmed_id,
                                        pd.date as pubmed_date from ct_drug cd
                                 full outer join pm_drug pd on pd.journal=cd.journal and pd.drug = cd.drug""")
        # data load
        res.toPandas().to_json(Utilities.get_path(self.spark_conf['OutputFile']), orient='records')
        self.stop_spark()

    def extract_journal_with_most_drugs(self) -> None:
        """
        This method aims to retrieve the journal that references the maximum of different drugs
        :return: None
        """

        # check if there is a spark session
        if self.spark:
            spark = self.start_spark_session()
        else:
            spark = self.spark

        # read the output json of run method
        data = spark.read.json(Utilities.get_path(self.spark_conf['OutputFile']))
        data.createOrReplaceTempView("data")

        # get the journals that referenced the max of different drugs
        res = spark.sql("""select tbl.*
                            from (
                                select journal, count(distinct drug) as drug_count
                                from data 
                                group by journal) tbl
                            where tbl.drug_count = (select max(t.drug_count) 
                                                    from (select journal, count(distinct drug) as drug_count
                                                        from data 
                                                        group by journal) t)
                            """)
        print(res.select("journal").collect())
        res.show()

        spark.stop()

    def start_spark_session(self):
        """
        :return: spark session
        """
        master = self.spark_conf['Master']
        app_name = self.spark_conf['AppName']
        spark = Utilities.start_spark(master, app_name)
        return spark

    def stop_spark(self):
        os.remove(self.clean_json)
        self.spark.stop()
