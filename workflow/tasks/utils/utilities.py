import ast
import datetime as dt
import json
import re
from pathlib import Path

import pandas as pd
from pyspark.sql import SparkSession
from pyspark.sql.types import StructField, StructType, StringType


class Utilities:

    @staticmethod
    def convert_date(date):
        """
        This method aims to get a unified format of date using different input formats
        :param date: input param date
        :return: datetime
        """
        date_str = str(date)
        if date_str.find('-') != -1:
            return dt.datetime.strptime(date, '%Y-%d-%m')
        else:
            if date_str.find('/') != -1:
                return dt.datetime.strptime(date, '%d/%m/%Y')
            else:
                return dt.datetime.strptime(date, '%d %B %Y')

    @staticmethod
    def json_cleaner_loader(path):
        """
        If json is corrupted, this method tries to remove trail commas in json object
        :param path: the path of json object
        """

        def clean_json(string):
            string = re.sub(',[ \t\r\n]+}', "}", string)
            string = re.sub(",[ \t\r\n]+\]", "]", string)
            return string

        with open(path) as fh:
            return json.dumps(clean_json("{}".format(fh.read())))

    @staticmethod
    def get_pandas_drug_reference(drugs_df, drug_col, lookup_df, lookup_col, output_cols):
        """
        this method retrieves from the dataframe lookup_df the rows where lookup_col column contains
        values of drug_col column from the drugs_df dataframe
        :param drugs_df: drugs pandas dataframe
        :param drug_col: drug column in drugs_df
        :param lookup_df: lookup pandas dataframe. Possible values are ct (clinical trials) or pm (pubmed)
        :param lookup_col: lookup column in lookup data frame (title column)
        :param output_cols: output columns except drug column of the output dataframe
        :return: pandas dataframe
        """

        def find_drug(drug, df_col):
            for title in df_col:
                yield drug.lower() in title.lower()

        res = pd.DataFrame()
        # loop on drugs dataframe
        for drug in drugs_df[drug_col]:
            c = lookup_df.copy()
            # filter lookup_df on rows containing only the drug
            c['filter'] = list(find_drug(drug, c[lookup_col]))
            c[drug_col] = drug
            res = res.append(c[c['filter'] == True][[drug_col] + output_cols])
        return res

    @staticmethod
    def spark_read_csv(spark, delimiter, path, header='true'):
        """
        read csv file in spark
        :param spark: spark session
        :param delimiter: csv delimiter
        :param path: file path
        :param header: using header if true
        :return: spark dataframe
        """
        return spark \
            .read \
            .format('csv') \
            .option('delimiter', delimiter) \
            .option('header', header) \
            .load(path)

    @staticmethod
    def spark_read_json(spark, path, id_col, title_col, date_col, journal_col, clean_json, multiLine=True):
        """
        read json in spark
        :param spark: spark session
        :param path: file path
        :param id_col: id column name
        :param title_col: title column name
        :param date_col: date column name
        :param journal_col: journal column name
        :param clean_json: temp file for saving the clean json
        :param multiLine: using multiline when reading the json
        :return: spark dataframe
        """
        string_type = StringType()
        schema = StructType([
            StructField(id_col, string_type, True),
            StructField(title_col, string_type, True),
            StructField(date_col, string_type, True),
            StructField(journal_col, string_type, True)
        ])
        # build clean json: remove trail comma
        json_obj = ast.literal_eval(json.loads(Utilities.json_cleaner_loader(path)))
        with open(clean_json, 'w') as outfile:
            json.dump(json_obj, outfile)
        # read cleaned json file
        res = spark.read.json(clean_json, schema, multiLine=multiLine).cache()
        return res

    @staticmethod
    def start_spark(master, app_name):
        """
        start spark session
        :param master: spark master
        :param app_name: spak app name
        """
        spark = SparkSession \
            .builder \
            .master(master) \
            .appName(app_name) \
            .getOrCreate()
        return spark

    @staticmethod
    def get_path(path):
        """
        return the absolute path using the relative path in param
        :param path: relative path from the root directory
        :return: string
        """
        root_dir = Path(__file__).parent.parent.parent.parent
        return str(root_dir) + path
