import configparser
from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.python_operator import PythonOperator, BranchPythonOperator
from airflow.operators.dummy_operator import DummyOperator
import logging
from workflow.tasks.etl.pandas_etl import PandasETL
from workflow.tasks.utils.utilities import Utilities

log = logging.getLogger(__name__)

one_days_ago = datetime.combine(datetime.today() - timedelta(1), datetime.min.time())
default = {
    'owner': 'pubmed',
    'depends_on_past': True,
    'start_date': one_days_ago,
    'email': ['maintainer@servier.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'end_date': datetime.today(),

    # dag custom params
    'conf_path': '/conf/conf.ini'
}


class PubmedETLDag(DAG):
    def __init__(self, default_args, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.default_args = default_args
        self.construct()

    def construct(self):
        with self:
            conf_path = self.default_args['conf_path']

            etl_run_task = PythonOperator(
                task_id='etl_run',
                python_callable=self._etl_run,
                provide_context=True,
                op_kwargs={'conf_path': conf_path})

            branch_task = BranchPythonOperator(
                task_id='branch_task',
                python_callable=self._branch_on_etl_output_file,
                provide_context=True)

            continue_task = DummyOperator(task_id='yes_keep_going')
            stop_task = DummyOperator(task_id='no_then_stop',
                                      python_callable=lambda: print("There is no output file in the etl pipeline"))

            get_journal_task = PythonOperator(
                task_id='get_journal',
                python_callable=self._etl_get_journal,
                provide_context=True,
                op_kwargs={'conf_path': conf_path})

            etl_run_task >> branch_task >> [continue_task, stop_task]
            continue_task >> get_journal_task

    def _branch_on_etl_output_file(self, **kwargs):
        # xcom_value =  kwargs['ti'].xcom_pull(task_ids='check_output_file', dag_id=self.dag_id)
        config = configparser.ConfigParser()
        config.read(Utilities.get_path(self.default_args['conf_path']))

        conf = config['DEFAULT']
        try:
            open(Utilities.get_path(conf['OutputFile']), 'r')
            return 'yes_keep_going'
        except NameError:
            return 'no_then_stop'

    def _etl_run(self, conf_path, **kwargs):
        config = configparser.ConfigParser()
        config.read(Utilities.get_path(conf_path))
        PandasETL(config).run()
        return

    def _etl_get_journal(self, conf_path, **kwargs):
        config = configparser.ConfigParser()
        config.read(Utilities.get_path(conf_path))
        PandasETL(config).extract_journal_with_most_drugs()
        return


dag = PubmedETLDag(dag_id='pubmed-etl-dag', default_args=default, schedule_interval="@daily")
