SELECT client_id,
SUM(CASE WHEN product_type = 'MEUBLE' THEN ventes THEN 0 END) as ventes_meuble,
SUM(CASE WHEN product_type = 'DECO' THEN ventes THEN 0 END) as ventes_deco
FROM (SELECT client_id,product_type,  SUM(prod_price*prod_qty) as ventes
     FROM TRANSACTION tr
     INNER JOIN PRODUCT_NOMENCLATURE pn ON tr.prod_id=pn.product_id
     WHERE year(tr.date) = 2019
     GROUP BY  client_id, product_type
      )
GROUP BY client_id