SELECT date, sum(prod_price*prod_qty) as ventes
FROM TRANSACTION tr
WHERE year(tr.date) = 2019
GROUP BY date
ORDER BY date