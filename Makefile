lint:
	flake8 --config tox.ini;

test:
	PYTHONPATH=. pytest tests/