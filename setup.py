from setuptools import find_packages, setup

setup(name='pubmed',
      version='1.0',
      description='An ETL pipeline',
      author='author',
      author_email='maintainer@servier.com',
      packages=find_packages(),
      install_requires=['pandas', 'pyspark', 'wheel', 'apache-airflow'],
      python_requires='>=3.6', )
