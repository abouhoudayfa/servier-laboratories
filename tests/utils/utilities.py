"""
utilities   _test.py
~~~~~~~~~~~~~~~

This module contains unit tests for the transformation steps of the ETL
job defined in pandas_etl.py.
"""
import unittest

import datetime

from workflow.tasks.utils.utilities import Utilities


class UtilitiesTests(unittest.TestCase):
    """Test suite for transformation in pandas_etl.py
    """

    def setUp(self):
        """define config and path to test data
        """
        self.utils = Utilities()

    def tearDown(self):
        """
        """
        pass

    def test_convert_date(self):
        """Test date converter.

        Using dummy date on input data and expected output data, we
        test the conversion step to make sure it's working as
        expected.
        """
        # assemble

        '2020-01-01'
        expected_data = datetime.datetime(2022, 5, 19)
        actual_data_format1 = self.utils.convert_date('19 May 2022')
        actual_data_format2 = self.utils.convert_date('19/05/2022')

        # assert
        self.assertEqual(expected_data, actual_data_format1)
        self.assertEqual(expected_data, actual_data_format2)