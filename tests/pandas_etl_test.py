"""
pandas_etl_test.py
~~~~~~~~~~~~~~~

This module contains unit tests for the transformation steps of the ETL
job defined in pandas_etl.py.
"""
import unittest

import pandas as pd

from workflow.tasks.etl.pandas_etl import PandasETL
import configparser

from workflow.tasks.utils.utilities import Utilities


class PandasETLTests(unittest.TestCase):
    """Test suite for transformation in pandas_etl.py
    """

    def setUp(self):
        """define config and path to test data
        """
        config = configparser.ConfigParser()
        config.read(Utilities.get_path('/tests/conf.ini'))
        self.output_file = config['DEFAULT']['OutputFile']
        self.json_orient = config['DEFAULT']['JsonOrient']
        self.etl = PandasETL(config)

    def tearDown(self):
        """
        """

    def test_transform_data(self):
        """Test data transformer.

        Using small chunks of input data and expected output data, we
        test the transformation step to make sure it's working as
        expected.
        """
        # assemble
        self.etl.run()
        actual_data = pd.read_json(Utilities.get_path(self.output_file), orient=self.json_orient)
        expected_data = pd.read_json(Utilities.get_path('/output/pandas/output.json'), orient=self.json_orient)

        expected_cols = len(expected_data.columns)
        expected_rows = expected_data.count()

        cols = len(actual_data.columns)
        rows = actual_data.count()
        # assert
        self.assertEqual(expected_cols, cols)
        self.assertTrue([row in expected_rows
                         for row in rows])
        self.assertTrue([col in expected_data.columns
                         for col in actual_data.columns])
