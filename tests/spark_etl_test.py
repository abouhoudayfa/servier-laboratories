"""
spark_etl_test.py
~~~~~~~~~~~~~~~

This module contains unit tests for the transformation steps of the ETL
job defined in spark_etl.py.
"""
import configparser
import unittest

from workflow.tasks.etl.spark_etl import SparkETL
from workflow.tasks.utils.utilities import Utilities


class SparkETLTests(unittest.TestCase):
    """Test suite for transformation in spark_etl.py
    """

    def setUp(self):
        """define config and path to test data
        """
        config = configparser.ConfigParser()
        config.read(Utilities.get_path('/tests/conf.ini'))
        self.conf = config
        self.output_file = config['spark.conf']['OutputFile']
        self.master = config['spark.conf']['Master']
        self.app_name = config['spark.conf']['AppName']

    def tearDown(self):
        """
        """

    def test_transform_data(self):
        """Test data transformer.

        Using small chunks of input data and expected output data, we
        test the transformation step to make sure it's working as
        expected.
        """
        # assemble
        SparkETL(self.conf).run()
        spark = Utilities.start_spark(self.master, self.app_name)
        actual_data = spark.read.json(Utilities.get_path(self.output_file))
        expected_data = spark.read.json(Utilities.get_path('/output/spark/output.json'))
        expected_rows = expected_data.count()
        rows = actual_data.count()
        # assert
        self.assertEqual(expected_rows, rows)
        spark.stop()
