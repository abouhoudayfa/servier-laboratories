# Project description
This project implements an etl data pipeline using pandas and pyspark to handle high volume of data.
To switch from one mode to another, please refer to conf.ini file in conf directory and change the value of 'UseSpark'.

This etl pipeline is defined in etl.py file and implemented in pandas_etl.py/spark_etl.py.

## Run principal program:
install dependencies:
```shell script
pip install -r requirements.txt
```
add the project root path to PYTHONPATH env var:
````shell script
set PYTHONPATH=%PYTHONPATH%;C:\path\to\the\project
````

and run this command line in project root directoty:
````shell script
python main.py
````

## Packaging
````shell script
python setup.py sdist bdist_wheel
````

## ETL functions:
```python
def run(self) -> None:
    """
    This method implements the transform and load steps in this ETL class
    and loads the result in a json file (OutputFile in the conf)
    :return: None
    """
    pass

def extract_journal_with_most_drugs(self) -> None:
    """
    This method aims to retrieve the journal that references the maximum of different drugs
    and loads the results in the console
    :return: None
    """
    pass
```

## Testing:
Unit tests can be launched, using this command line in project root directory:
```shell script
python -m unittest discover ./tests "*_test.py"
```

## Use Airflow
Use airflow to schedule the etl pipeline:  <br />
    *) PythonOperator: to call run method of the etl after instantiating it <br />
    *) PythonBranchOperator: to see if the output json file exists  <br />
    *) DummyOperator: to stop the airflow dag if there is no output json file available  <br />
    *) PythonOperator: to call extract_journal_with_most_drugs method (after instantiating the etl) to get the journals with most referenced drugs on the console  <br />
To do this, we need to create a docker image of this project using:
```shell script
docker build --rm -t pubmed/airflow:1.10.9 .
```
And run this mage on top of airflow local executor, by launching the local executor docker:
```shell script
docker-compose -f docker-compose-LocalExecutor.yml up -d
```

## SQL part
The second part of this test is implemented in sql directory, in files question1.sql and question2.sql


## Improvements
1/ In this project, the spark etl is a good fit to handle big data challenges and is tested in local mode. <br />
It can be used when the data workload increses up to TB. But it should be configured on a cluster (EMR for example, or DataProc).  

2/ Use Parquet format for spark instead of json, and compressed file format (.gz)

3/ Generate fat package for spark and use spark submit to run spark etl on airflow

4/ Use the ELT paradigm to handle data by using BashOperator To load data to bigQuery (bq load) Then use BigQueryOperator to transform data by wiriting the business logic on a sql file;
```python
from airflow.contrib.operators.bigquery_operator import BigQueryOperator

transform_data = BigQueryOperator(
    task_id='transform_data',
    sql=os.path.join('sql', f"pubmed", 'query.sql'),
    use_legacy_sql=False,
    destination_dataset_table=TARGET_TABLE,
    write_disposition='WRITE_TRUNCATE',
    allow_large_results=True,
    bigquery_conn_id='bigquery_conn',
    dag=MAIN_DAG)
```